﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Yandex_API
{
    public class YandexAPI
    {
        /// <summary>
        /// Method to get the token for user of your application
        /// </summary>
        /// <param name="login">Login in yandex of user which token you want to take</param>
        /// <param name="password">Users password for his yandex accaunt</param>
        /// <param name="applicationClientId">id of your application registered at yandex</param>
        /// <returns>Token for working of your application with user</returns>
        public static string GetToken(string login, string password, string applicationClientId)
        {
            string Cookies = WebRequests.GetYandexCookie();

            string AuthCookies = WebRequests.GetYandexLoginingCookie(Cookies, applicationClientId, login, password);

            WebResponse responce = WebRequests.GetTokenAuthPage(AuthCookies, applicationClientId);

            if (responce.Headers["Location"] != null)
            {
                string res = responce.Headers["Location"].Split(new char[] { '#', '&' })[1].Split(new char[] { '=' })[1];
                return res;
            }

            string reqText = ResponceHandling.ParceTokenAuth(responce);            

            WebResponse tokenResponce = WebRequests.GetTokenResponce(AuthCookies, applicationClientId, reqText);

            string token = ResponceHandling.GetToken(tokenResponce);

            return token;
        }

        /// <summary>
        /// Method that download picter to users album. Be sure that this user have public album (album with at least one public picter)
        /// </summary>
        /// <param name="token">Token for your application and user</param>
        /// <param name="OwnersUrl">URL to profile of user. Should be represented in form: "http://api-fotki.yandex.ru/api/users/userName/"</param>
        /// <param name="photoPath">full path to file</param>
        /// <param name="photoName">title to photo</param>
        /// <param name="photoType">the type of file you want to download should be on of this: "image/bmp", "image/gif", "image/jpeg", "image/png"</param>
        public static void DownloadPhoto(string token, string OwnersUrl, string photoPath, string photoName, string photoType)
        {
            string[] strArr = new string[] { "image/bmp", "image/gif", "image/jpeg", "image/png" };
            if (!strArr.Contains(photoType))
                throw new BadImageFormatException("The type of photo was impermissible");
            
            string albumUrl = GetAlbum(OwnersUrl);

            WebResponse responce = WebRequests.DownloadPhoto(albumUrl, token, photoType, photoPath);

            string photoEditUrl = ResponceHandling.GetPhotoEditURL(responce);

            responce = WebRequests.GetPhotoEdit(photoEditUrl, token);

            var info = ResponceHandling.ChangePhotosInfo(responce, photoName);

            WebRequests.ChangePhotosInfo(photoEditUrl, token, info);
        }

        /// <summary>
        /// Method to get all photos from one of users albums. Be sure that this user have public album (album with at least one public picter)
        /// </summary>
        /// <param name="Url">List of links to photos</param>
        /// <param name="Name">List of titles to photos</param>
        /// <param name="OwnersUrl">URL to profile of user. Should be represented in form: "http://api-fotki.yandex.ru/api/users/userName/</param>
        /// <param name="token">Token for your application and user</param>
        public static void GetPhoto(out List<String> Url, out List<String> Name, string OwnersUrl, string token = null)
        {
            Url = new List<string>();
            Name = new List<string>();

            var getPhotosUrl = GetAlbum(OwnersUrl);

            while (getPhotosUrl != null)
            {
                var responce = WebRequests.GetPhotos(getPhotosUrl, token);

                XmlNodeList set;
                XmlNamespaceManager nsmgr;
                ResponceHandling.GetPhotoInfo(out set, out getPhotosUrl, out nsmgr, responce);
                
                foreach (var entry in set)
                {
                    var c = new XmlDocument();
                    c.LoadXml(((XmlElement)entry).OuterXml);
                    Name.Add(c.SelectSingleNode("/app:entry/app:title", nsmgr).InnerText);
                    Url.Add(c.SelectSingleNode("/app:entry/f:img[@size='S']", nsmgr).Attributes["href"].Value);
                }
            }
        }

        private static string GetAlbum(string OwnersUrl)
        {
            string albumUrl;
            try
            {
                var albumUrlQuery = LoadDataFromApi(OwnersUrl, "/app:service/app:workspace/app:collection[@id='album-list']");

                albumUrl = LoadDataFromApi(albumUrlQuery, "/app:feed/app:entry/app:link[@rel='photos']");
            }
            catch (NullReferenceException)
            {
                throw new Exception("there is no availible albums");
            }
            return albumUrl;
        }

        private static string LoadDataFromApi(string Url, string xPathString, string attribute = "href")
        {
            var responce = WebRequests.SimpleRequest(Url);

            return ResponceHandling.GetSimpleInfo(responce, xPathString, attribute);
        }
    }
}
