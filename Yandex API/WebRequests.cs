﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Drawing;
using System.Xml;

namespace Yandex_API
{
    internal class WebRequests
    {
        private static UTF8Encoding encoding = new UTF8Encoding();
        private const string DefaultUserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36";
        private const string DefaultAccept = "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*";
        internal static string GetYandexCookie()
        {
            var request = (HttpWebRequest)WebRequest.Create("http://yandex.ru");
            request.UserAgent = DefaultUserAgent;
            request.Accept = DefaultAccept;
            request.Headers.Add("Accept-Language", "ru");
            var responce = (HttpWebResponse)request.GetResponse();
            string sCookies = "";
            if (!String.IsNullOrEmpty(responce.Headers["Set-Cookie"]))
            {
                sCookies = responce.Headers["Set-Cookie"];
            }
            return sCookies;
        }

        internal static string GetYandexLoginingCookie(string sCookies, string applicationClientId, string login, string password)
        {
            var requestLogining = (HttpWebRequest)WebRequest.Create("https://passport.yandex.ru/auth");
            requestLogining.Method = "POST";
            requestLogining.Referer = "http://yandex.ru";
            requestLogining.UserAgent = DefaultUserAgent;
            requestLogining.Accept = DefaultAccept;
            requestLogining.Headers.Add("Accept-Language", "ru");
            requestLogining.ContentType = "application/x-www-form-urlencoded";
            if (!String.IsNullOrEmpty(sCookies))
            {
                requestLogining.Headers.Add(HttpRequestHeader.Cookie, sCookies);
            }
            requestLogining.AllowAutoRedirect = false;
            TimeSpan ts = DateTime.Now - new DateTime(1970, 1, 1);
            string sQuery = "retpath=https%3A%2F%2Foauth.yandex.ru%2Fauthorize%3Fresponse_type%3Dtoken%26client_id%" + applicationClientId + "%2F&timestamp=" + Math.Floor(ts.TotalSeconds).ToString() + "&login=" + login + "&passwd=" + password;
            requestLogining.ContentLength = encoding.GetByteCount(sQuery);
            using (Stream requestStream = requestLogining.GetRequestStream())
            {
                requestStream.Write(encoding.GetBytes(sQuery), 0, encoding.GetByteCount(sQuery));
            }
            var respLogining = requestLogining.GetResponse();
            var sCookiesAuthorised = respLogining.Headers["Set-Cookie"];
            return sCookiesAuthorised;
        }

        internal static WebResponse GetTokenAuthPage(string AuthCookies, string applicationClientId)
        {
            var request = (HttpWebRequest)WebRequest.Create("https://oauth.yandex.ru/authorize?response_type=token&client_id=" + applicationClientId);
            request.UserAgent = DefaultUserAgent;
            request.Accept = DefaultAccept;
            request.Headers.Add("Accept-Language", "ru");
            request.Headers.Add(HttpRequestHeader.Cookie, AuthCookies);
            request.AllowAutoRedirect = false;
            var responce = request.GetResponse();
            return responce;
        }

        internal static WebResponse GetTokenResponce(string AuthCookies, string applicationClientId, string reqText)
        {
            var request = (HttpWebRequest)WebRequest.Create("https://oauth.yandex.ru/authorize/allow ");
            request.Method = "POST";
            request.UserAgent = DefaultUserAgent;
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Referer = "https://oauth.yandex.ru/authorize?response_type=token&client_id=" + applicationClientId;
            request.Headers.Add("Accept-Language", "ru,en;q=0.8");
            request.Headers.Add(HttpRequestHeader.Cookie, AuthCookies);
            request.ContentLength = encoding.GetByteCount(reqText);
            request.AllowAutoRedirect = false;
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(encoding.GetBytes(reqText), 0, encoding.GetByteCount(reqText));
            }

            return request.GetResponse();
        }

        internal static WebResponse DownloadPhoto(string albumUrl, string token, string photoType, string photoPath)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(albumUrl);
            request.Method = "POST";
            request.Headers.Add("Authorization", "OAuth " + token);
            request.ContentType = photoType;

            Image image = Image.FromFile(photoPath);
            ImageConverter conv = new ImageConverter();
            byte[] arr = (byte[])conv.ConvertTo(image, typeof(byte[]));
            request.ContentLength = arr.Length;
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(arr, 0, arr.Length);
            }
            image.Dispose();
            System.Net.ServicePointManager.Expect100Continue = false;

            return request.GetResponse();
        }

        internal static WebResponse GetPhotoEdit(string photoEditUrl, string token)
        {
            var request = WebRequest.Create(photoEditUrl);
            request.Headers.Add("Authorization", "OAuth " + token);
            return request.GetResponse();
        }

        internal static void ChangePhotosInfo(string photoEditUrl, string token, XmlDocument info)
        {
            HttpWebRequest requestChange = (HttpWebRequest)WebRequest.Create(photoEditUrl);
            requestChange.Method = "PUT";
            requestChange.Headers.Add("Authorization", "OAuth " + token);
            requestChange.ContentType = "application/atom+xml; charset=utf-8; type=entry";
            using (Stream requestStream = requestChange.GetRequestStream())
            {
                UTF8Encoding encoding = new UTF8Encoding();
                string k = info.ToString();
                requestStream.Write(encoding.GetBytes(info.OuterXml), 0, encoding.GetByteCount(info.OuterXml));
            }
            var responce = requestChange.GetResponse();
        }

        internal static WebResponse GetPhotos(string getPhotosUrl, string token)
        {
            var requestPhotos = WebRequest.Create(getPhotosUrl);
            requestPhotos.ContentType = "application/atom+xml; charset=utf-8; type=feed";
            if (token != null)
                requestPhotos.Headers.Add("Authorization", "OAuth " + token);
            requestPhotos.Method = "GET";
            return requestPhotos.GetResponse();
        }

        internal static WebResponse SimpleRequest(string Url)
        {
            var request = WebRequest.Create(Url);
            return request.GetResponse();
        }
    }
}
