﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace Yandex_API
{
    internal class ResponceHandling
    {
        internal static string ParceTokenAuth(WebResponse responce)
        {
            var streamGet = responce.GetResponseStream();
            var strGet = new StreamReader(streamGet, Encoding.GetEncoding(1251));
            string sGet = strGet.ReadToEnd();
            strGet.Close();

            string csrf = GetSubstring(sGet, @"<input type=""hidden"" name=""csrf"" value=""[a-z0-9]+""/>").Split(new char[] { ' ' })[3].Split(new char[] { '"' })[1];
            string request_id = GetSubstring(sGet, @"<input type=""hidden"" name=""request_id"" value=""[a-z0-9]+""/>").Split(new char[] { ' ' })[3].Split(new char[] { '"' })[1];
            string reqText = "csrf=" + csrf + "&response_type=token&redirect_uri=http%3A%2F%2Flocalhost%3A53396%2F&request_id=" + request_id;
            return reqText;
        }

        internal static string GetToken(WebResponse responce)
        {
            string token = responce.Headers["Location"].Split(new char[] { '#', '&' })[1].Split(new char[] { '=' })[1];
            return token;
        }

        internal static string GetPhotoEditURL(WebResponse responce)
        {
            var streamGet = responce.GetResponseStream();
            var strGet = new StreamReader(streamGet);
            var sGet = strGet.ReadToEnd();
            strGet.Close();

            var photoInfo = new XmlDocument();
            photoInfo.LoadXml(sGet);

            var nsmgr = new XmlNamespaceManager(photoInfo.NameTable);
            nsmgr.AddNamespace("app", photoInfo.DocumentElement.NamespaceURI);
            string photoEditUrl = photoInfo.SelectSingleNode("/app:entry/app:link[@rel='edit']", nsmgr).Attributes["href"].Value;

            return photoEditUrl;
        }

        internal static XmlDocument ChangePhotosInfo(WebResponse responce, string photoName)
        {
            var streamGet = responce.GetResponseStream();
            var strGet = new StreamReader(streamGet);
            var sGet = strGet.ReadToEnd();
            strGet.Close();

            var info = new XmlDocument();
            info.LoadXml(sGet);

            var nsmgr = new XmlNamespaceManager(info.NameTable);
            nsmgr.AddNamespace("app", info.DocumentElement.NamespaceURI);
            info.SelectSingleNode("/app:entry/app:title", nsmgr).InnerText = photoName;
            return info;
        }

        internal static void GetPhotoInfo(out XmlNodeList set, out string getPhotosUrl, out XmlNamespaceManager nsmgr, WebResponse responce)
        {
            var streamGet = responce.GetResponseStream();
            var strGet = new StreamReader(streamGet);
            var sGet = strGet.ReadToEnd();
            strGet.Close();

            var photoSet = new XmlDocument();
            photoSet.LoadXml(sGet);

            nsmgr = new XmlNamespaceManager(photoSet.NameTable);
            nsmgr.AddNamespace("app", photoSet.DocumentElement.NamespaceURI);
            nsmgr.AddNamespace("f", "yandex:fotki");

            set = photoSet.SelectNodes("/app:feed/app:entry", nsmgr);

            var x = photoSet.SelectSingleNode("/app:feed/app:link[@rel='next']", nsmgr);
            getPhotosUrl = (x == null) ? null : x.Attributes["href"].Value;
        }

        internal static string GetSimpleInfo(WebResponse responce, string xPathString, string attribute)
        {
            var streamGet = responce.GetResponseStream();
            var strGet = new StreamReader(streamGet);
            string sGet = strGet.ReadToEnd();
            strGet.Close();

            var Doc = new XmlDocument();
            Doc.LoadXml(sGet);

            var nsmgr = new XmlNamespaceManager(Doc.NameTable);
            nsmgr.AddNamespace("app", Doc.DocumentElement.NamespaceURI);
            string result = Doc.SelectSingleNode(xPathString, nsmgr).Attributes[attribute].Value;

            return result;
        }

        private static string GetSubstring(string sGet, string pattern)
        {
            Regex reg = new Regex(pattern);
            MatchCollection m = reg.Matches(sGet);
            string res = null;
            if (m.Count > 0)
                res = m[0].Value;
            return res;
        }
    }
}
