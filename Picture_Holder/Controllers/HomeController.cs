﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Picture_Holder.Models;
using Yandex_API;

namespace Picture_Holder.Controllers
{
    public class HomeController : Controller
    {
        private const string Login = "t.stain@yandex.ru";
        private const string Password = "pq1Hte4bD";
        private const string ClientId = "1805a93890c845ac8fd9977a4d8ce4a6";

        private const string OwnersUrl = "http://api-fotki.yandex.ru/api/users/t.stain/";

        private string NameChanger(string inp)
        {
            string res = inp;
            bool b = true;
            while (b)
            {
                if (res[res.Length - 1] == '.')
                    b = false;
                res = res.Remove(res.Length - 1);
            }
            return res;
        }

        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                string token = YandexAPI.GetToken(Login, Password, ClientId);

                string booly = Request.QueryString["Error"];
                string connFail = Request.QueryString["Connection"];
                MainModel m = new MainModel();
                m.Error = ((booly != null) && (booly == "true")) ? "visible" : "hidden";

                if (connFail == null)
                {
                    YandexAPI.GetPhoto(out m.Url, out m.Name, OwnersUrl, token);
                }
                else
                {
                    m.connFail = "visible";
                }
                return View(m);
            }
            catch(Exception)
            {
                return Redirect("?Connection=false");
            }
        }

        [HttpPost]
        public RedirectResult Index(HttpPostedFileBase fileUpload)
        {
            try
            {
                string[] strArr = new string[] { "image/bmp", "image/gif", "image/jpeg", "image/png" };
                if ((fileUpload != null) && strArr.Contains(fileUpload.ContentType))
                {
                    string token = YandexAPI.GetToken(Login, Password, ClientId);

                    var fileName = string.Copy(Path.GetFileName(fileUpload.FileName));
                    var path = Path.Combine(Server.MapPath("~/files/"), fileName);
                    fileUpload.SaveAs(path);

                    YandexAPI.DownloadPhoto(token, OwnersUrl, path, NameChanger(fileName), fileUpload.ContentType);

                    System.IO.File.Delete(path);

                    return Redirect("Home");
                }
                else
                {
                    return Redirect("Home?Error=true");
                }
            }
            catch (Exception)
            {
                return Redirect("?Connection=false");
            }
        }
    }
}